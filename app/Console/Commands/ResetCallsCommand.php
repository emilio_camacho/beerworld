<?php

namespace App\Console\Commands;

use App\Http\Adapter\Api\IApiRepository;
use Illuminate\Console\Command;

class ResetCallsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:reset:calls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset calls for general API';

    /**
     * @var IApiRepository
     */
    private $apiRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IApiRepository $apiRepository)
    {
        parent::__construct();
        $this->apiRepository = $apiRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api = $this->apiRepository->getById(env("API_NAME"));
        $api->setNumCalls(0);
        $this->apiRepository->save($api);
    }
}
