<?php

namespace App\Console\Commands;

use App\Http\Adapter\Api\IApiRepository;
use App\Http\Entity\Api;
use Illuminate\Console\Command;

class SeedApiCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:register';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Register brewery Api';

    /**
     * @var IApiRepository
     */
    private $apiRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IApiRepository $apiRepository)
    {
        parent::__construct();
        $this->apiRepository = $apiRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api = new Api();
        $api->setName('Beer');
        $api->setId(env("API_NAME"));
        $api->setDescription('Brewery api');
        $api->setLimit(400);
        $api->setNumCalls(0);
        $this->apiRepository->save($api);
    }
}
