<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 23:16
 */

namespace App\Http\Adapter\Api;


use App\Http\Adapter\IEntityRepository;
use App\Http\Entity\Api;

interface IApiRepository extends IEntityRepository
{

}