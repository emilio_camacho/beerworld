<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:20
 */

namespace App\Http\Adapter\Beer;


use App\Http\Adapter\IEntityRepository;
use App\Http\Entity\Beer;
use App\Http\Entity\Brewery;

interface IBeerRepository extends IEntityRepository
{
    /**
     * Get a random beer
     * @return Beer
     */
    public function getRandom();

}