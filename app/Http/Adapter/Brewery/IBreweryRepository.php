<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:20
 */

namespace App\Http\Adapter\Brewery;


use App\Http\Adapter\IEntityRepository;

interface IBreweryRepository extends IEntityRepository
{

}