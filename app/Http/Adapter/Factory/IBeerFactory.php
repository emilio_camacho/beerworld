<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 14:46
 */

namespace App\Http\Adapter\Factory;


use App\Http\Entity\Beer;
use App\Http\Entity\Brewery;

interface IBeerFactory
{
    /**
     * Create a beer
     * @param string $id
     * @param string $name
     * @param string $description
     * @param string $icon
     * @param string $image
     * @paran Brewery $brewery
     * @param float $abv
     * @param float $ibu
     * @param boolean $organic
     * @param integer $status
     * @param \DateTime $createdAt
     * @return Beer
     */
    static public function createBeer(
        $id,
        $name,
        $description,
        $icon,
        $image,
        Brewery $brewery,
        $abv = null,
        $ibu = null,
        $organic = null,
        $status = null,
        \DateTime $createdAt = null
    );
}