<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 18:11
 */

namespace App\Http\Adapter\Factory;


use App\Http\Entity\Brewery;

interface IBreweryFactory
{
    /**
     * Create a brewery
     * @param string $id
     * @param string $name
     * @param string $description
     * @param string $icon
     * @param string $image
     * @param integer $status
     * @param \DateTime $createdAt
     * @return Brewery
     */
    static public function createBrewery(
        $id,
        $name,
        $description,
        $icon,
        $image,
        $status,
        \DateTime $createdAt
    );
}