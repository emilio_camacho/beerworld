<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:34
 */

namespace App\Http\Adapter;


use App\Http\Entity\AbstractEntity;

interface IEntityRepository
{
    /**
     * Persist an entity
     * @param AbstractEntity $entity
     */
    public function save(AbstractEntity $entity);

    /**
     * Get an entity
     * @param $id
     * @return AbstractEntity
     */
    public function getById($id);

    /**
     * Get all entity which match with the requested name
     * @param $name
     * @return AbstractEntity[]
     */
    public function getByName($name);
}