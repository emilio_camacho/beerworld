<?php

namespace App\Http\Controllers;

use App\Http\Adapter\Beer\IBeerRepository;
use App\Http\Repository\API\BeerApiRepository;
use App\Http\UseCase\Beer\Get\GetBeerRequest;
use App\Http\UseCase\Beer\Get\GetBeerUseCase;
use App\Http\UseCase\Beer\GetRandom\GetRandomBeerRequest;
use App\Http\UseCase\Beer\GetRandom\GetRandomBeerUseCase;
use App\Http\UseCase\Brewery\Get\GetBreweryRequest;
use App\Http\UseCase\Brewery\Get\GetBreweryUseCase;
use App\Http\UseCase\IUseCase;
use GuzzleHttp\RequestOptions;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;

class BeerController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var IUseCase
     */
    private $getRandomBeerUseCase;

    /**
     * @var IUseCase
     */
    private $getBeerUseCase;

    /**
     * @var IUseCase
     */
    private $getBreweryUseCase;

    /**
     * BeerController constructor.
     * @param GetRandomBeerUseCase $getRandomBeerUseCase
     * @param GetBeerUseCase $getBeerUseCase
     * @param GetBreweryUseCase $getBreweryUseCase
     */
    public function __construct(
        GetRandomBeerUseCase $getRandomBeerUseCase,
        GetBeerUseCase $getBeerUseCase,
        GetBreweryUseCase $getBreweryUseCase
    ) {
        $this->getRandomBeerUseCase = $getRandomBeerUseCase;
        $this->getBeerUseCase = $getBeerUseCase;
        $this->getBreweryUseCase = $getBreweryUseCase;
    }

    /**
     * Display a random beer and list of "brother" beers
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $beer = null;
        $brewery = null;
        $errors = null;
        try {
            $responseBeer = $this->getRandomBeerUseCase->execute(
                new GetRandomBeerRequest()
            );
            $beer = $responseBeer->getBeer();
            $brewery = null;

            if ($beer){
                $responseBrewery = $this->getBreweryUseCase->execute(
                    new GetBreweryRequest($beer->getBrewery()->getId())
                );
                if (!empty($responseBrewery->getBreweries())) {
                    $brewery = $responseBrewery->getBreweries()[0];
                }
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }

        return view('page.beer',
            [
                'beer' => $beer,
                'brewery' => $brewery,
                'errors' => $errors
            ]
        );
    }

    /**
     * Display a beer
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function beer(Request $request, $id)
    {
        $beer = null;
        $brewery = null;
        $errors = null;
        try {
            $response = $this->getBeerUseCase->execute(new GetBeerRequest($id));

            $beer = null;
            $brewery = null;

            if (!empty($response->getBeers())) {
                $beer = $response->getBeers()[0];
                $responseBrewery = $this->getBreweryUseCase->execute(
                    new GetBreweryRequest($beer->getBrewery()->getId())
                );
                if (!empty($responseBrewery->getBreweries())) {
                    $brewery = $responseBrewery->getBreweries()[0];
                }
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }
        return view('page.beer',
            [
                'beer' => $beer,
                'brewery' => $brewery,
                'errors' => $errors
            ]
        );
    }

    /**
     * Display a list of beers which match with requested name
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function beerList(Request $request)
    {
        $beers = null;
        $messages = null;
        $errors = null;

        try {
            $validator = Validator::make(
                ['beer' => $request->get('name_beer')],
                ['beer' => 'required|required|regex:/^[\pL\s\-]+$/u|max:50']
            );
            if ($validator->passes()) {
                $response = $this->getBeerUseCase->execute(
                    new GetBeerRequest(null, $request->get('name_beer'))
                );
                $beers = $response->getBeers();
            } else {
                $messages = $validator->messages()->getMessages();
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }



        return view('page.beers',
            [
                'beers' => $beers,
                'search' => $request->get('name_beer'),
                'messages' => $messages,
                'errors' => $errors
            ]
        );
    }

    /**
     * Display a brewery
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function brewery(Request $request, $id)
    {
        $brewery = null;
        $errors = null;
        try {
            $responseBrewery = $this->getBreweryUseCase->execute(
                new GetBreweryRequest($id)
            );
            if (!empty($responseBrewery->getBreweries())) {
                $brewery = $responseBrewery->getBreweries()[0];
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }

        return view (
            'page.brewery',
            ['brewery' => $brewery, 'errors' => $errors]
        );
    }

    /**
     * Display a brewery list which match with the search
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function breweryList(Request $request)
    {
        $breweries = null;
        $messages = null;
        $errors = null;

        try {
            $validator = Validator::make(
                ['brewery' => $request->get('name_brewery')],
                ['brewery' => 'required|required|regex:/^[\pL\s\-]+$/u|max:50']
            );

            if ($validator->passes()) {
                $response = $this->getBreweryUseCase->execute(
                    new GetBreweryRequest(null, $request->get('name_brewery'))
                );
                $breweries = $response->getBreweries();
            } else {
                $messages = $validator->messages()->getMessages();
            }
        } catch (\Exception $e) {
            $errors[] = $e->getMessage();
        }

        return view('page.breweries',
            [
                'breweries' => $breweries,
                'search' => $request->all()['name_brewery'],
                'messages' => $messages,
                'errors' => $errors
            ]
        );
    }
}
