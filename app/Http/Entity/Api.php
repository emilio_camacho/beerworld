<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 23:19
 */

namespace App\Http\Entity;


class Api extends AbstractEntity
{
    /**
     * @var integer
     */
    private $limit;

    /**
     * @var integer
     */
    private $numCalls;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $name;

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param mixed $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return mixed
     */
    public function getNumCalls()
    {
        return $this->numCalls;
    }

    /**
     * @param mixed $numCalls
     */
    public function setNumCalls($numCalls)
    {
        $this->numCalls = $numCalls;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function incrementCall()
    {
        $this->numCalls = ($this->numCalls) ? $this->numCalls + 1 : 1;
    }
}