<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 14:09
 */

namespace App\Http\Entity;


class Beer extends AbstractEntity
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $abv;

    /**
     * @var float
     */
    private $ibu;

    /**
     * @var boolean
     */
    private $organic;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var string
     */
    private $icon;

    /**
     * @var string
     */
    private $image;

    /**
     * @var Brewery
     */
    private $brewery;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getAbv()
    {
        return $this->abv;
    }

    /**
     * @param float $abv
     */
    public function setAbv($abv)
    {
        $this->abv = $abv;
    }

    /**
     * @return float
     */
    public function getIbu()
    {
        return $this->ibu;
    }

    /**
     * @param float $ibu
     */
    public function setIbu($ibu)
    {
        $this->ibu = $ibu;
    }

    /**
     * @return boolean
     */
    public function isOrganic()
    {
        return $this->organic;
    }

    /**
     * @param boolean $organic
     */
    public function setOrganic($organic)
    {
        $this->organic = $organic;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return Brewery
     */
    public function getBrewery()
    {
        return $this->brewery;
    }

    /**
     * @param Brewery $brewery
     */
    public function setBrewery($brewery)
    {
        $this->brewery = $brewery;
    }


}