<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 14:50
 */

namespace App\Http\Factory;


use App\Http\Adapter\Factory\IBeerFactory;
use App\Http\Entity\Beer;
use App\Http\Entity\Brewery;

class BeerFactory implements IBeerFactory
{
    /**
     * @inheritdoc
     */
    static public function createBeer(
        $id,
        $name,
        $description,
        $icon,
        $image,
        Brewery $brewery,
        $abv = null,
        $ibu = null,
        $organic = null,
        $status = null,
        \DateTime $createdAt = null
    ) {
        $beer = new Beer();
        $beer->setId($id);
        $beer->setName($name);
        $beer->setDescription($description);
        $beer->setAbv($abv);
        $beer->setIbu($ibu);
        $beer->setOrganic($organic == 'Y' ? true : false);
        $beer->setStatus($status);
        $beer->setIcon($icon);
        $beer->setImage($image);
        $beer->setCreatedAt($createdAt);
        $beer->setUpdatedAt(new \DateTime());
        $beer->setBrewery($brewery);
        return $beer;
    }
}