<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 18:15
 */

namespace App\Http\Factory;


use App\Http\Adapter\Factory\IBreweryFactory;
use App\Http\Entity\Brewery;

class BreweryFactory implements IBreweryFactory
{
    /**
     * @inheritdoc
     */
    static public function createBrewery(
        $id,
        $name,
        $description,
        $icon,
        $image,
        $status,
        \DateTime $createdAt
    )
    {
        $brewery = new Brewery();
        $brewery->setId($id);
        $brewery->setName($name);
        $brewery->setDescription($description);
        $brewery->setIcon($icon);
        $brewery->setImage($image);
        $brewery->setStatus($status);
        $brewery->setCreatedAt($createdAt);
        $brewery->setUpdatedAt(new \DateTime());
        return $brewery;
    }
}