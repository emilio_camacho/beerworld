<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 14:23
 */

namespace App\Http\Repository\API;


use App\Http\Adapter\Api\IApiRepository;
use App\Http\Entity\AbstractEntity;
use App\Http\Entity\Beer;
use App\Http\Factory\BeerFactory;
use App\Http\Factory\BreweryFactory;
use App\Http\Entity\Brewery;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

class AbstractApiRepository
{
    const ENDPOINT = 'http://api.brewerydb.com/v2/';

    const SUCCESS_CODE = 200;

    const SUCCESS_STATUS = 'success';

    const FIELD_RESULTS = 'totalResults';
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_ORGANIC = 'isOrganic';
    const FIELD_STATUS = 'status';
    const FIELD_CREATED_AT = 'createDate';
    const FIELD_ICON = 'icon';
    const FIELD_IMAGE = 'medium';
    const FIELD_DATA = 'data';
    const FIELD_IMAGES_BREWERY = 'images';
    const FIELD_ABV = 'abv';
    const FIELD_IBU = 'ibu';
    const FIELD_IMAGES_BEER = 'labels';
    const FIELD_BREWERIES = 'breweries';

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var ClientInterface
     */
    protected $httpClient;

    /**
     * @var IApiRepository
     */
    protected $apiRepository;

    /**
     * AbstractApiRepository constructor.
     * @param $apiKey
     * @param ClientInterface $httpClient
     * @param IApiRepository $apiRepository
     */
    public function __construct(
        $apiKey,
        ClientInterface $httpClient,
        IApiRepository $apiRepository
    ) {
        $this->apiKey = $apiKey;
        $this->httpClient = $httpClient;
        $this->apiRepository = $apiRepository;
    }

    /**
     * Save in api not implemented
     * @param AbstractEntity $entity
     * @return null
     */
    public function save(AbstractEntity $entity)
    {
        return null;
    }

    /**
     * Make a request to get results from the API
     * @param string $option
     * @param array $params
     * @param string $method
     * @return array|null
     */
    protected function doRequest($option, array $params, $method = 'GET')
    {
        $baseParams = ["key" => $this->apiKey];
        $url = self::ENDPOINT . $option . "?" .
            http_build_query(array_merge($baseParams, $params));

        try {
            $options = [RequestOptions::HEADERS => ["Content-Type" => "application/json"]];
            $response = $this->httpClient->request($method, $url, $options);
            $this->incrementApiCall();
            if ($response->getStatusCode() != self::SUCCESS_CODE) {
                Log::error("Unexpected responde from beer api: code ". $response->getStatusCode());
                return null;
            }
            return json_decode($response->getBody()->getContents(), true);

        } catch (\Exception $e) {
                Log::error("Error in request");
            return null;
        }
    }

    /**
     * Check if an array containt a key (this can be nested)
     * if not return null
     * @param array $result
     * @param array|string $field
     * @return array|null
     */
    protected function returnFieldOrNull(array $result, $field)
    {
        if (is_array($field)) {

            foreach ($field as $checkKey) {
                if (isset($result[$checkKey])) {
                    $result = $result[$checkKey];
                } else {
                    return null;
                }
            }
            return $result;
        } else {
            return isset($result[$field]) ? $result[$field] : null;
        }
    }

    /**
     * Create A brewery from the result from the API
     * @param array $breweryRaw
     * @return Brewery
     */
    protected function createBrewery($breweryRaw)
    {
        return BreweryFactory::createBrewery(
            $breweryRaw[self::FIELD_ID],
            $breweryRaw[self::FIELD_NAME],
            $this->returnFieldOrNull($breweryRaw, self::FIELD_DESCRIPTION),
            $this->returnFieldOrNull(
                $breweryRaw,
                [self::FIELD_IMAGES_BREWERY, self::FIELD_ICON]
            ),
            $this->returnFieldOrNull(
                $breweryRaw,
                [self::FIELD_IMAGES_BREWERY, self::FIELD_IMAGE]
            ),
            $this->returnFieldOrNull($breweryRaw,[self::SUCCESS_STATUS]),
            new \DateTime(
                $this->returnFieldOrNull(
                    $breweryRaw,
                    self::FIELD_CREATED_AT
                )
            )
        );
    }

    /**
     * Create a beer from the result from the APi
     * @param array $beerRaw
     * @param Brewery $brewery
     * @return Beer
     */
    protected function createBeer($beerRaw, Brewery $brewery)
    {
        return BeerFactory::createBeer(
            $beerRaw[self::FIELD_ID],
            $beerRaw[self::FIELD_NAME],
            $beerRaw[self::FIELD_DESCRIPTION],
            $beerRaw[self::FIELD_IMAGES_BEER][self::FIELD_ICON],
            $beerRaw[self::FIELD_IMAGES_BEER][self::FIELD_IMAGE],
            $brewery,
            $this->returnFieldOrNull($beerRaw, self::FIELD_ABV),
            $this->returnFieldOrNull($beerRaw, self::FIELD_IBU),
            $this->returnFieldOrNull($beerRaw, self::FIELD_ORGANIC),
            $this->returnFieldOrNull($beerRaw, self::FIELD_STATUS),
            new \DateTime(
                $this->returnFieldOrNull(
                    $beerRaw,
                    self::FIELD_CREATED_AT
                )
            )
        );
    }

    /**
     * Check if the result from the API is correct
     * @param array $result
     * @return bool
     */
    protected function validResult(array $result)
    {
        return $result[self::FIELD_STATUS] == self::SUCCESS_STATUS
            && isset($result[self::FIELD_DATA]);
    }

    /**
     * Get only beers that match with our criterial
     * (Contain name, description and images)
     * @param array $posibleRawBeers
     * @return Beer[]
     */
    protected function getBeersFromPossibleRawBeers(array $posibleRawBeers, Brewery $brewery = null)
    {

        $rawBeers = array_filter(
            $posibleRawBeers,
            function ($possible) {
                return isset(
                    $possible[self::FIELD_NAME],
                    $possible[self::FIELD_DESCRIPTION],
                    $possible[self::FIELD_IMAGES_BEER]
                );
            }
        );
        if (!empty($rawBeers)) {
            return array_map(function ($beerRaw) use ($brewery) {
                if (!$brewery) {
                    $breweryRaw = $beerRaw[self::FIELD_BREWERIES][0];
                    $brewery = $this->createBrewery($breweryRaw);
                }
                return $this->createBeer($beerRaw, $brewery);
            }, $rawBeers);
        }
        return [];
    }

    /**
     * Incremente api calls
     */
    protected function incrementApiCall()
    {
        $api = $this->apiRepository->getById(env("API_NAME"));
        $api->incrementCall();
        $this->apiRepository->save($api);
    }
}