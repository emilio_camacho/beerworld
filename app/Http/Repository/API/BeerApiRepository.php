<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 14:10
 */

namespace App\Http\Repository\API;


use App\Http\Adapter\Beer\IBeerRepository;

class BeerApiRepository extends AbstractApiRepository implements IBeerRepository
{

    /**
     * @inheritdoc
     */
    public function getByName($name)
    {
        $result = $this->doRequest(
            'beers',
            [
                'name' => $name,
                'withBreweries' => 'Y',
                'hasLabels' => 'Y'
            ]
        );
        if ($this->validResult($result)) {
            return $this->getBeersFromPossibleRawBeers(
                $result[self::FIELD_DATA]
            );
        } else {
            return [];
        }
    }

    /**
     * @inheritdoc
     */
    public function getById($id)
    {
        $result = $this->doRequest(
            'beer/' . $id,
            ['withBreweries' => 'Y']
        );

        if ($this->validResult($result)) {
            $beerRaw = $result[self::FIELD_DATA];
            $breweryRaw = $beerRaw[self::FIELD_BREWERIES][0];
            $brewery = $this->createBrewery($breweryRaw);
            return $this->createBeer($beerRaw, $brewery);
        }
    }

    /**
     * @inheritdoc
     */
    public function getRandom()
    {
        $result = $this->doRequest(
            'beer/random',
            [
                'hasLabels' => 'Y',
                'withBreweries' => 'Y'
            ]
        );

        if ($this->validResult($result)) {
            $beerRaw = $result[self::FIELD_DATA];
            if (isset(
                $beerRaw[self::FIELD_NAME],
                $beerRaw[self::FIELD_DESCRIPTION],
                $beerRaw[self::FIELD_IMAGES_BEER],
                $beerRaw[self::FIELD_BREWERIES]
                )
            ) {
                $breweryRaw = $beerRaw[self::FIELD_BREWERIES][0];
                $brewery = $this->createBrewery($breweryRaw);
                return $this->createBeer($beerRaw, $brewery);
            } else {
                return $this->getRandom();
            }
        } else {
            return $this->getRandom();
        }
    }
}