<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 14:11
 */

namespace App\Http\Repository\API;


use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\Entity\Brewery;

class BreweryApiRepository extends AbstractApiRepository implements IBreweryRepository
{
    /**
     * @inheritdoc
     */
    public function getById($id)
    {
        $result = $this->doRequest('brewery/' . $id, []);
        if ($this->validResult($result)) {
            return $this->getBreweryFromRawBrewery($result[self::FIELD_DATA]);
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getByName($name)
    {
        $result = $this->doRequest('breweries/', ['name' => $name, 'hasImages' => 'Y']);
        if ($this->validResult($result)) {
            $breweries = [];
            foreach ($result[self::FIELD_DATA] as $rawBrewery) {
                $breweries[] = $this->getBreweryFromRawBrewery($rawBrewery);
            }
            return $breweries;
        }
        return [];
    }

    /**
     * Get a brewery object from the result returned by the api
     * @param array $rawBrewery
     * @return Brewery
     */
    private function getBreweryFromRawBrewery(array $rawBrewery)
    {
        $breweryRaw = $rawBrewery;

        $brewery = $this->createBrewery($breweryRaw);
        $beers = $this->getBeersFromBrewery($brewery);
        if (!empty($beers)) {
            foreach ($beers as $beer) {
                $brewery->addBeer($beer);
            }
        }
        return $brewery;
    }

    /**
     * Get all beers from a brewery
     * @param Brewery $brewery
     * @return Beer[]
     */
    private function getBeersFromBrewery(Brewery $brewery)
    {
        $result = $this->doRequest('/brewery/'.$brewery->getId().'/beers', []);
        if ($this->validResult($result)) {
           return $this->getBeersFromPossibleRawBeers($result[self::FIELD_DATA], $brewery);
        }
        return [];
    }
}