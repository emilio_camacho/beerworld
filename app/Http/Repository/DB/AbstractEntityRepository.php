<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 20:20
 */

namespace App\Http\Repository\DB;


use App\Http\Entity\AbstractEntity;
use Doctrine\ORM\EntityRepository;

class AbstractEntityRepository extends EntityRepository
{
    /**
     * @inheritdoc
     */
    public function getById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public function getByName($name)
    {
        return $this->findBy(['name'=>$name]);
    }

    /**
     * @inheritdoc
     */
    public function save(AbstractEntity $entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }
}