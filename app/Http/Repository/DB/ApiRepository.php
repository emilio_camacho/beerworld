<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 15:07
 */

namespace App\Http\Repository\DB;


use App\Http\Adapter\Api\IApiRepository;
use App\Http\Entity\AbstractEntity;
use Doctrine\ORM\EntityRepository;

class ApiRepository extends AbstractEntityRepository implements IApiRepository
{

}