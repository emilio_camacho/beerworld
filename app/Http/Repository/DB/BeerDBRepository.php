<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 20:24
 */

namespace App\Http\Repository\DB;


use App\Http\Adapter\Beer\IBeerRepository;

class BeerDBRepository extends AbstractEntityRepository implements IBeerRepository
{
    /**
     * @inheritdoc
     */
    public function getRandom()
    {
        $numBeers = $this->createQueryBuilder('b')
            ->select('COUNT(b.id)')
            ->getQuery()
            ->getSingleScalarResult();

        if ($numBeers > 1) {
            $randNum = rand(0, $numBeers - 1);

            $qb = $this->createQueryBuilder('b')
            ->getQuery()
            ->setFirstResult($randNum)
            ->setMaxResults(1)
            ->getResult();
            if ($qb) {
                return $qb[0];
            }
        }
        return null;
    }
}