<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 20:24
 */

namespace App\Http\Repository\DB;


use App\Http\Adapter\Brewery\IBreweryRepository;

class BreweryDBRepository extends AbstractEntityRepository implements IBreweryRepository
{

}