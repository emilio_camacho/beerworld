<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:08
 */

namespace App\Http\UseCase;


class AbstractUseCaseException extends \RuntimeException
{
    /**
     * Internal use case related exception code
     * @var int
     */
    private $internalCode;

    /**
     * @param string $internalCode
     * @param int $message
     */
    public function __construct($internalCode, $message)
    {
        parent::__construct($message);
        $this->internalCode = $internalCode;
    }

    /**
     * @return int
     */
    public function getInternalCode()
    {
        return $this->internalCode;
    }

    /**
     * @param int $internalCode
     */
    public function setInternalCode($internalCode)
    {
        $this->internalCode = $internalCode;
    }
}