<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 23:34
 */

namespace App\Http\UseCase\Api\Get;


use App\Http\UseCase\AbstractUseCaseException;

class GetApiException extends AbstractUseCaseException
{
    const API_ID_MISSING = 1;

    const API_NOT_FOUND = 2;
}