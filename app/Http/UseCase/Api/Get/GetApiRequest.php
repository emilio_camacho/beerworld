<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 23:35
 */

namespace App\Http\UseCase\Api\Get;


use App\Http\UseCase\IUseCaseRequest;

class GetApiRequest implements IUseCaseRequest
{
    /**
     * @var string
     */
    private $apiId;

    /**
     * GetApiRequest constructor.
     * @param $apiId
     */
    public function __construct($apiId)
    {
        $this->apiId = $apiId;
    }

    /**
     * @return mixed
     */
    public function getApiId()
    {
        return $this->apiId;
    }

}