<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 23:35
 */

namespace App\Http\UseCase\Api\Get;


use App\Http\Entity\Api;
use App\Http\UseCase\IUseCaseResponse;

class GetApiResponse implements IUseCaseResponse
{
    /**
     * @var Api
     */
    private $api;

    /**
     * GetApiResponse constructor.
     * @param Api $api
     */
    public function __construct(Api $api)
    {
        $this->api = $api;
    }

    /**
     * @return Api
     */
    public function getApi()
    {
        return $this->api;
    }


}