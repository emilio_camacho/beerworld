<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 23:35
 */

namespace App\Http\UseCase\Api\Get;


use App\Http\Adapter\Api\IApiRepository;
use App\Http\UseCase\IUseCase;
use App\Http\UseCase\IUseCaseRequest;

class GetApiUseCase implements IUseCase
{
    /**
     * @var IApiRepository
     */
    private $apiRepository;

    /**
     * GetApiUseCase constructor.
     * @param IApiRepository $apiRepository
     */
    public function __construct(IApiRepository $apiRepository)
    {
        $this->apiRepository = $apiRepository;
    }

    /**
     * Get an Api
     * @param IUseCaseRequest $request
     * @return GetApiResponse
     */
    public function execute(IUseCaseRequest $request)
    {
        if (!$request->getApiId()) {
            throw new GetApiException(
                GetApiException::API_ID_MISSING,
                "API id missing"
            );
        }
        $api = $this->apiRepository->getById($request->getApiId());
        if(!$api) {
            throw new GetApiException(
                GetApiException::API_NOT_FOUND,
                "Api: ".$request->getApiId()." not found"
            );
        }
        return new GetApiResponse($api);
    }
}