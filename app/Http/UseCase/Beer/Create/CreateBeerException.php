<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:05
 */

namespace App\Http\UseCase\Beer\Create;

use App\Http\UseCase\AbstractUseCaseException;

class CreateBeerException extends AbstractUseCaseException
{
    const WRONG_REQUEST = 1;

    const BEER_ALREADY_EXISTS = 2;
}