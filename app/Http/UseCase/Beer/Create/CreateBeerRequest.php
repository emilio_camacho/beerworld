<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:05
 */

namespace App\Http\UseCase\Beer\Create;


use App\Http\Entity\Beer;
use App\Http\UseCase\IUseCaseRequest;

class CreateBeerRequest implements IUseCaseRequest
{
    /**
     * @var Beer
     */
    private $beer;

    /**
     * @var boolean
     */
    private $forceUpdate;

    /**
     * CreateBeerRequest constructor.
     * @param Beer $beer
     * @param bool $forceUpdate
     */
    public function __construct(Beer $beer, $forceUpdate = false)
    {
        $this->beer = $beer;
        $this->forceUpdate = $forceUpdate;
    }

    /**
     * @return Beer
     */
    public function getBeer()
    {
        return $this->beer;
    }

    /**
     * @return boolean
     */
    public function isForceUpdate()
    {
        return $this->forceUpdate;
    }

}