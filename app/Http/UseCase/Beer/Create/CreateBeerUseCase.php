<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:06
 */

namespace App\Http\UseCase\Beer\Create;


use App\Http\Adapter\Beer\IBeerRepository;
use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\Entity\Beer;
use App\Http\UseCase\Beer\Get\GetBeerRequest;
use App\Http\UseCase\IUseCase;
use App\Http\UseCase\IUseCaseRequest;

class CreateBeerUseCase implements IUseCase
{
    /**
     * @var IBeerRepository
     */
    private $beerRepository;

    /**
     * @var IBreweryRepository
     */
    private $breweryRepository;

    /**
     * @var IUseCase
     */
    private $getBreweryUseCase;

    /**
     * CreateBeerUseCase constructor.
     * @param IBeerRepository $beerRepository
     * @param IBreweryRepository $breweryRepository
     * @param IUseCase $getBreweryUseCase
     */
    public function __construct(
        IBeerRepository $beerRepository,
        IBreweryRepository $breweryRepository,
        IUseCase $getBreweryUseCase
    ){
        $this->beerRepository = $beerRepository;
        $this->breweryRepository = $breweryRepository;
        $this->getBreweryUseCase = $getBreweryUseCase;
    }

    /**
     * Persist a beer
     * @param IUseCaseRequest $request
     */
    public function execute(IUseCaseRequest $request)
    {
        if (!$this->checkAlreadyExists(
            $request->getBeer(),
            $request->isForceUpdate()
        )) {
            /**
             * Instead of create just this beer, we create all beers for this brewery
             */
            $this->getBreweryUseCase->execute(
                new GetBeerRequest($request->getBeer()->getBrewery()->getId())
            );
        }
    }

    /**
     * Check if the beer has been persisted before
     * @param Beer $beer
     * @param $forceUpdate
     * @return bool
     */
    private function checkAlreadyExists(Beer $beer, $forceUpdate)
    {
        if (!$forceUpdate && $this->beerRepository->getById($beer->getId())) {
            return true;
        }
        return false;
    }

}