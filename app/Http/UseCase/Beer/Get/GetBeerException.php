<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:40
 */

namespace App\Http\UseCase\Beer\Get;


use App\Http\UseCase\AbstractUseCaseException;

class GetBeerException extends AbstractUseCaseException
{
    const PARAMETER_REQUIRED = 1;
}