<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:40
 */

namespace App\Http\UseCase\Beer\Get;


use App\Http\UseCase\IUseCaseRequest;

class GetBeerRequest implements IUseCaseRequest
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string | null
     */
    private $name;

    /**
     * GetBeerRequest constructor.
     * @param string $id
     * @param string $name
     */
    public function __construct($id = null, $name = null)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

}