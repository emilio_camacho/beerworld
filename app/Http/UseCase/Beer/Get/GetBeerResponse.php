<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:40
 */

namespace App\Http\UseCase\Beer\Get;


use App\Http\Entity\Beer;
use App\Http\UseCase\IUseCaseResponse;

class GetBeerResponse implements IUseCaseResponse
{
    /**
     * @var Beer[]
     */
    private $beers;

    /**
     * GetBeerResponse constructor.
     * @param Beer[] $beers
     */
    public function __construct(array $beers)
    {
        $this->beers = $beers;
    }
    /**
     * @return Beer[]
     */
    public function getBeers()
    {
        return $this->beers;
    }

}