<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:40
 */

namespace App\Http\UseCase\Beer\Get;


use App\Http\Adapter\Api\IApiRepository;
use App\Http\Adapter\Beer\IBeerRepository;
use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\Entity\Beer;
use App\Http\UseCase\Api\Get\GetApiRequest;
use App\Http\UseCase\Beer\Create\CreateBeerRequest;
use App\Http\UseCase\IUseCase;
use App\Http\UseCase\IUseCaseRequest;

class GetBeerUseCase implements IUseCase
{
    /**
     * @var IBeerRepository
     */
    private $beerDBRepository;

    /**
     * @var IBeerRepository
     */
    private $beerApiRepository;

    /**
     * @var IUseCase
     */
    private $createBeerUseCase;

    /**
     * @var IUseCase
     */
    private $getApiUseCase;

    /**
     * @var string
     */
    private $apiId;

    /**
     * GetBeerUseCase constructor.
     * @param IBeerRepository $beerDBRepository
     * @param IBeerRepository $beerApiRepository
     * @param IUseCase $createBeerUseCase
     * @param IUseCase $getApiUseCase
     * @param $apiId
     */
    public function __construct(
        IBeerRepository $beerDBRepository,
        IBeerRepository $beerApiRepository,
        IUseCase $createBeerUseCase,
        IUseCase $getApiUseCase,
        $apiId
    ){
        $this->beerDBRepository = $beerDBRepository;
        $this->beerApiRepository = $beerApiRepository;
        $this->createBeerUseCase = $createBeerUseCase;
        $this->getApiUseCase = $getApiUseCase;
        $this->apiId = $apiId;
    }

    /**
     * Get all beers that match with the search
     * @param IUseCaseRequest $request
     * @return GetBeerResponse
     */
    public function execute(IUseCaseRequest $request)
    {
        if (!$request->getName() && !$request->getId()) {
            throw new GetBeerException(
                GetBeerException::PARAMETER_REQUIRED,
                "Missing parameter"
            );
        }

        return new GetBeerResponse(
            $this->getBeers($request->getId(), $request->getName())
        );
    }

    /**
     * Try to get beers first from the DB if not from the API
     * @param string | null $id
     * @param string | null $name
     * @return Beer[] | null
     */
    private function getBeers($id = null, $name = null)
    {

        $apiResponse = $this->getApiUseCase->execute(
            new GetApiRequest($this->apiId)
        );
        $api = $apiResponse->getApi();

        $beers = $this->getBeersFromDB($id, $name);
        if(empty($beers) && $api->getNumCalls() < $api->getLimit()) {
            $beers = $this->getBeersFromApi($id, $name);
            $this->createBeers($beers);
        }
        return $beers;
    }

    /**
     * Try to get beers from the DB
     * @param string|null $id
     * @param string|null $name
     * @return Beer[]
     */
    private function getBeersFromDB($id = null, $name = null)
    {

        if ($id) {
            $beer = $this->beerDBRepository->getById($id);
            $beers = $beer ? [$beer] : [];
        } else {
            $beers = $this->beerDBRepository->getByName($name);
        }
        return $beers;
    }

    /**
     * Try to get beers from the api
     * @param string|null $id
     * @param string|null $name
     * @return Beer[]
     */
    private function getBeersFromApi($id = null, $name = null)
    {
        if ($id) {
            $beer = $this->beerApiRepository->getById($id);
            $beers = $beer ? [$beer] : [];
        } else {
            $beers = $this->beerApiRepository->getByName($name);
        }
        return $beers;
    }
    /**
     * persist beers if they come from the API
     * @param Beer[] $beers
     */
    private function createBeers(array $beers)
    {
        if (!empty($beers)) {
            foreach ($beers as $beer) {
                $request = new CreateBeerRequest($beer);
                $this->createBeerUseCase->execute($request);
            }
        }
    }

}