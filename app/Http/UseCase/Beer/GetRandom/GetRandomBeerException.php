<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 22:51
 */

namespace App\Http\UseCase\Beer\GetRandom;


use App\Http\UseCase\AbstractUseCaseException;

class GetRandomBeerException extends AbstractUseCaseException
{
    const RANDOM_BEER_NOT_FOUND = 1;
}