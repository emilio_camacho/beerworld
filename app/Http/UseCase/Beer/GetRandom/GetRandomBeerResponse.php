<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 22:52
 */

namespace App\Http\UseCase\Beer\GetRandom;


use App\Http\Entity\Beer;
use App\Http\UseCase\IUseCaseResponse;

class GetRandomBeerResponse implements IUseCaseResponse
{
    /**
     * @var Beer
     */
    private $beer;

    /**
     * GetRandomBeerResponse constructor.
     * @param Beer $beer
     */
    public function __construct(Beer $beer)
    {
        $this->beer = $beer;
    }

    /**
     * @return Beer
     */
    public function getBeer()
    {
        return $this->beer;
    }

}