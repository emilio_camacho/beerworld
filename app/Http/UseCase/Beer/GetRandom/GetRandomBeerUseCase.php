<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 22:51
 */

namespace App\Http\UseCase\Beer\GetRandom;


use App\Http\Adapter\Api\IApiRepository;
use App\Http\Adapter\Beer\IBeerRepository;
use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\Entity\Beer;
use App\Http\Enum\API;
use App\Http\UseCase\Api\Get\GetApiRequest;
use App\Http\UseCase\Beer\Create\CreateBeerRequest;
use App\Http\UseCase\IUseCase;
use App\Http\UseCase\IUseCaseRequest;

class GetRandomBeerUseCase implements IUseCase
{
    /**
     * @var IBeerRepository
     */
    private $beerApiRepository;

    /**
     * @var IBeerRepository
     */
    private $beerBDRepository;

    /**
     * @var IUseCase
     */
    private $createUseCase;

    /**
     * @var IUseCase
     */
    private $getApiUseCase;


    /**
     * GetRandomBeerUseCase constructor.
     * @param IBeerRepository $beerApiRepository
     * @param IBeerRepository $beerBDRepository
     * @param IUseCase $createUseCase
     * @param IUseCase $getApiUseCase
     * @param $apiId
     */
    public function __construct(
        IBeerRepository $beerApiRepository,
        IBeerRepository $beerBDRepository,
        IUseCase $createUseCase,
        IUseCase $getApiUseCase,
        $apiId
    ) {
          $this->beerApiRepository = $beerApiRepository;
          $this->beerBDRepository = $beerBDRepository;
          $this->createUseCase = $createUseCase;
          $this->getApiUseCase =  $getApiUseCase;
          $this->apiId = $apiId;
    }

    /**
     * If there is enoght api calls, it gets a random beer from the API,
     * if not it gets the random beer from the DB
     * @param IUseCaseRequest $request
     * @return GetRandomBeerResponse
     */
    public function execute(IUseCaseRequest $request)
    {
        $apiResponse = $this->getApiUseCase->execute(
            new GetApiRequest($this->apiId)
        );
        $api = $apiResponse->getApi();

        if ($api->getNumCalls() < $api->getLimit()) {
            $beer = $this->beerApiRepository->getRandom();
            if ($beer && !$this->beerBDRepository->getById($beer->getId())) {
                $this->createUseCase->execute(new CreateBeerRequest($beer));
            }
        } else {
            $beer = $this->beerBDRepository->getRandom();
        }
        if (!$beer) {
            throw new GetRandomBeerException(
                GetRandomBeerException::RANDOM_BEER_NOT_FOUND,
                "Random beer not found"
            );
        }
        return new GetRandomBeerResponse($beer);
    }
}