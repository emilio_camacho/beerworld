<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 23:59
 */

namespace App\Http\UseCase\Brewery\Create;


use App\Http\UseCase\AbstractUseCaseException;

class CreateBreweryException extends AbstractUseCaseException
{
    const WRONG_REQUEST = 1;

    const BREWERY_ALREADY_EXISTS = 2;
}