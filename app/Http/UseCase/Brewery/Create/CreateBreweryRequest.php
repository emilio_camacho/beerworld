<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 0:00
 */

namespace App\Http\UseCase\Brewery\Create;


use App\Http\Entity\Brewery;
use App\Http\UseCase\IUseCaseRequest;

class CreateBreweryRequest implements IUseCaseRequest
{
    /**
     * @var Brewery
     */
    private $brewery;

    /**
     * @var boolean
     */
    private $forceUpdate;

    /**
     * CreateBreweryRequest constructor.
     * @param Brewery $brewery
     * @param bool $forceUpdate
     */
    public function __construct(Brewery $brewery, $forceUpdate = false)
    {
        $this->brewery = $brewery;
        $this->forceUpdate = $forceUpdate;
    }

    /**
     * @return Brewery
     */
    public function getBrewery()
    {
        return $this->brewery;
    }

    /**
     * @return boolean
     */
    public function isForceUpdate()
    {
        return $this->forceUpdate;
    }

}