<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 0:00
 */

namespace App\Http\UseCase\Brewery\Create;

use App\Http\Adapter\Beer\IBeerRepository;
use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\Entity\Brewery;
use App\Http\UseCase\IUseCase;
use App\Http\UseCase\IUseCaseRequest;

class CreateBreweryUseCase implements IUseCase
{
    /**
     * @var IBreweryRepository
     */
    private $breweryRepository;

    /**
     * @var IBeerRepository
     */
    private $beerRepository;

    /**
     * CreateBreweryUseCase constructor.
     * @param IBreweryRepository $breweryRepository
     * @param IBeerRepository $beerRepository
     */
    public function __construct(
        IBreweryRepository $breweryRepository,
        IBeerRepository $beerRepository
    ) {
        $this->breweryRepository = $breweryRepository;
        $this->beerRepository = $beerRepository;
    }

    /**
     * Create a brewery and every beer from this brewery
     * @param IUseCaseRequest $request
     */
    public function execute(IUseCaseRequest $request)
    {
        if (!$this->checkAlreadyExists(
            $request->getBrewery(),
            $request->isForceUpdate()
        )) {
            $this->checkBeersInBreweryExist($request->getBrewery());
            $this->breweryRepository->save($request->getBrewery());
        }


    }

    /**
     * Check if the brewery alerady exists in the DB
     * @param Brewery $brewery
     * @param $forceUpdate
     */
    private function checkAlreadyExists(Brewery $brewery, $forceUpdate)
    {
        if (!$forceUpdate && $this->breweryRepository->getById($brewery->getId())) {
           return true;
        }
        return false;
    }

    /**
     * Beers which previusly existed in BD replace beers from API
     * @param Brewery $brewery
     */
    private function checkBeersInBreweryExist(Brewery $brewery)
    {
        $beers = [];
        foreach ($brewery->getBeers() as $beer) {
            $beerInDb = $this->beerRepository->getById($beer->getId());
            if ($beerInDb) {
                $beers[] = $beerInDb;
            } else {
                $beers[] = $beer;
            }
        }
        $brewery->setBeers($beers);
    }
}