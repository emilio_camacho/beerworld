<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 0:29
 */

namespace App\Http\UseCase\Brewery\Get;


use App\Http\UseCase\AbstractUseCaseException;

class GetBreweryException extends AbstractUseCaseException
{
    const PARAMETER_REQUIRED = 1;
}