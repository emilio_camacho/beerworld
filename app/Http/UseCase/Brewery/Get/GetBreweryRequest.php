<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 0:29
 */

namespace App\Http\UseCase\Brewery\Get;


use App\Http\UseCase\IUseCaseRequest;

class GetBreweryRequest implements IUseCaseRequest
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * GetBreweryRequest constructor.
     * @param null $id
     * @param null $name
     */
    public function __construct($id = null, $name = null)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


}