<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 0:29
 */

namespace App\Http\UseCase\Brewery\Get;


use App\Http\Entity\Brewery;
use App\Http\UseCase\IUseCaseResponse;

class GetBreweryResponse implements IUseCaseResponse
{
    /**
     * @var Brewery[]
     */
    private $breweries;

    /**
     * GetBreweryResponse constructor.
     * @param array $breweries
     */
    public function __construct(array $breweries)
    {
        $this->breweries = $breweries;
    }

    /**
     * @return Brewery[]
     */
    public function getBreweries()
    {
        return $this->breweries;
    }

}