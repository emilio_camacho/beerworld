<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 0:30
 */

namespace App\Http\UseCase\Brewery\Get;


use App\Http\Adapter\Api\IApiRepository;
use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\Entity\Brewery;
use App\Http\UseCase\Api\Get\GetApiRequest;
use App\Http\UseCase\Brewery\Create\CreateBreweryRequest;
use App\Http\UseCase\IUseCase;
use App\Http\UseCase\IUseCaseRequest;

class GetBreweryUseCase implements IUseCase
{
    /**
     * @var IBreweryRepository
     */
    private $breweryApiRepository;

    /**
     * @var IBreweryRepository
     */
    private $breweryDBRepository;

    /**
     * @var IUseCase
     */
    private $createBreweryUseCase;

    /**
     * @var IUseCase
     */
    private $getApiUseCase;

    /**
     * @var IApiRepository
     */
    private $apiRepository;

    /**
     * @var string
     */
    private $apiId;

    /**
     * GetBreweryUseCase constructor.
     * @param IBreweryRepository $breweryApiRepository
     * @param IBreweryRepository $breweryDBRespository
     * @param IUseCase $createBreweryUseCase
     * @param IUseCase $getApiUseCase
     * @param IApiRepository $apiRepository
     * @param $apiId
     */
    public function __construct(
        IBreweryRepository $breweryApiRepository,
        IBreweryRepository $breweryDBRespository,
        IUseCase $createBreweryUseCase,
        IUseCase $getApiUseCase,
        IApiRepository $apiRepository,
        $apiId
    ) {
        $this->breweryApiRepository = $breweryApiRepository;
        $this->breweryDBRepository = $breweryDBRespository;
        $this->createBreweryUseCase = $createBreweryUseCase;
        $this->apiRepository = $apiRepository;
        $this->getApiUseCase = $getApiUseCase;
        $this->apiId = $apiId;
    }

    /**
     * Get Breweries which match the search
     * @param IUseCaseRequest $request
     */
    public function execute(IUseCaseRequest $request)
    {
        if (!$request->getName() && !$request->getId()) {
            throw new GetBreweryException(
                GetBreweryException::PARAMETER_REQUIRED,
                "Missing parameter"
            );
        }
        return new GetBreweryResponse(
            $this->getBreweries($request->getId(), $request->getName())
        );
    }

    /**
     * Try to get breweries from DB if not from API
     * @param string | null $id
     * @param string | null $name
     * @return Brewery | null
     */
    private function getBreweries($id = null, $name = null)
    {
        $apiResponse = $this->getApiUseCase->execute(
            new GetApiRequest($this->apiId)
        );
        $api = $apiResponse->getApi();
        $breweries = $this->getBreweriesFromDB($id, $name);


        if(empty($breweries) && $api->getNumCalls() < $api->getLimit()) {
            $breweries = $this->getBreweriesFromApi($id, $name);
            $this->createBreweries($breweries);
        }
        return $breweries;
    }

    /**
     * Try to get breweries from DB
     * @param string|null $id
     * @param string|null $name
     * @return Brewery[]
     */
    private function getBreweriesFromDB($id = null, $name = null)
    {
        if ($id) {
            $brewery = $this->breweryDBRepository->getById($id);
            $breweries = $brewery ? [$brewery] : [];
        } else {
            $breweries = $this->breweryDBRepository->getByName($name);
        }
        return $breweries;
    }

    /**
     * Try to get breweries from api
     * @param string|null $id
     * @param string|null $name
     * @return Brewery[]
     */
    private function getBreweriesFromApi($id = null, $name = null)
    {
        if ($id) {
            $brewery = $this->breweryApiRepository->getById($id);
            $breweries = $brewery ? [$brewery] : [];
        } else {
            $breweries = $this->breweryApiRepository->getByName($name);
        }
        return $breweries;
    }

    /**
     * Create all Breweries
     * @param Brewery[] $breweries
     */
    private function createBreweries(array $breweries)
    {
        if (!empty($breweries)) {
            foreach ($breweries as $brewery) {
                $request = new CreateBreweryRequest($brewery);
                $this->createBreweryUseCase->execute($request);
            }
        }
    }
}