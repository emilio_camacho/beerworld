<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 26/08/16
 * Time: 15:11
 */

namespace App\Http\UseCase;

/**
 * Interface IUseCase
 * @package App\Http\UseCase
 */
interface IUseCase
{
    public function execute(IUseCaseRequest $request);
}