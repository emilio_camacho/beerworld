<?php
/*
Route::group(['middleware' => ['web']], function () {

    Route::match(
        ['get', 'post'],
        '/',
        'BeerController@index'
    )->name('index');

});
*/
Route::get('/', 'BeerController@index')->name('index');
Route::get('/beer/{id}', 'BeerController@beer')->name('beer');
Route::post('/beer/list', 'BeerController@beerList')->name('beerList');
Route::get('/brewery/{id}', 'BeerController@brewery')->name('brewery');
Route::post('/brewery/list', 'BeerController@breweryList')->name('breweryList');
