<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 15:01
 */

namespace App\Providers;


use App\Http\Entity\Api;
use App\Http\Entity\Beer;
use App\Http\Entity\Brewery;
use App\Http\Repository\API\BeerApiRepository;
use App\Http\Repository\API\BreweryApiRepository;
use App\Http\Repository\DB\BeerDBRepository;
use App\Http\Repository\DB\BreweryDBRepository;
use App\Http\Repository\DB\ApiRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;
use App\Http\Adapter\Api\IApiRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IApiRepository::class, function(){
            return new ApiRepository(
                app('registry')->getManager('default'),
                new ClassMetadata(Api::class)
            );
        });

        $this->app->singleton(BeerApiRepository::class, function() {
           return new BeerApiRepository(
               env('API_KEY'),
               app(ClientInterface::class),
               app(IApiRepository::class)
           );
        });

        $this->app->singleton(BreweryApiRepository::class, function() {
            return new BreweryApiRepository(
                env('API_KEY'),
                app(ClientInterface::class),
                app(IApiRepository::class)
            );
        });

        $this->app->singleton(BeerDBRepository::class, function(){
            return new BeerDBRepository(
                app('registry')->getManager('default'),
                new ClassMetadata(Beer::class)
            );
        });

        $this->app->singleton(BreweryDBRepository::class, function(){
            return new BreweryDBRepository(
                app('registry')->getManager('default'),
                new ClassMetadata(Brewery::class)
            );
        });
    }
}