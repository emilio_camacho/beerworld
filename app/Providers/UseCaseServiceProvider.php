<?php
/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 27/08/16
 * Time: 15:01
 */

namespace App\Providers;


use App\Http\Repository\API\BeerApiRepository;
use App\Http\Repository\API\BreweryApiRepository;
use App\Http\Repository\DB\BeerDBRepository;
use App\Http\Repository\DB\BreweryDBRepository;
use App\Http\UseCase\Beer\GetRandom\GetRandomBeerUseCase;
use App\Http\UseCase\Beer\Create\CreateBeerUseCase;
use App\Http\UseCase\Beer\Get\GetBeerUseCase;
use App\Http\UseCase\Api\Get\GetApiUseCase;
use App\Http\UseCase\Brewery\Create\CreateBreweryUseCase;
use App\Http\UseCase\Brewery\Get\GetBreweryUseCase;
use Illuminate\Support\ServiceProvider;
use App\Http\Adapter\Api\IApiRepository;

class UseCaseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CreateBeerUseCase::class, function (){
           return new CreateBeerUseCase(
               app(BeerDBRepository::class),
               app(BreweryDBRepository::class),
               app(GetBreweryUseCase::class)
           );
        });
        $this->app->singleton(GetApiUseCase::class, function (){
           return new GetApiUseCase(
               app(IApiRepository::class)
           );
        });
        $this->app->singleton(GetRandomBeerUseCase::class, function () {
            return new GetRandomBeerUseCase(
                app(BeerApiRepository::class),
                app(BeerDBRepository::class),
                app(CreateBeerUseCase::class),
                app(GetApiUseCase::class),
                env('API_NAME')
            );
        });

        $this->app->singleton(GetBeerUseCase::class, function () {
           return new  GetBeerUseCase(
               app(BeerDBRepository::class),
               app(BeerApiRepository::class),
               app(CreateBeerUseCase::class),
               app(GetApiUseCase::class),
               env('API_NAME')
           );
        });

        $this->app->singleton(CreateBreweryUseCase::class, function (){
            return new CreateBreweryUseCase(
                app(BreweryDBRepository::class),
                app(BeerDBRepository::class)
            );
        });
        $this->app->singleton(GetBreweryUseCase::class, function () {
            return new GetBreweryUseCase(
                app(BreweryApiRepository::class),
                app(BreweryDBRepository::class),
                app(CreateBreweryUseCase::class),
                app(GetApiUseCase::class),
                app(IApiRepository::class),
                env('API_NAME')
            );
        });
    }
}