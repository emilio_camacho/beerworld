# Beer World Project

Beer World Project is an app, which let you discover new Beers around the word

## Instalation
    
    Create and api key in http://www.brewerydb.com 
    Create a database in mysql
    copy .env.example in .env and fill it
    Run "php composer.phar install"
    Run “php artisan doctrine:schema:create”
    Run “php artisan doctrine:generate:proxies”
    Run "php artisan api:register”
    Create a cron job to run daily at 00:00 which run “php artisan api:reset:calls”
    Run "php artisan:serve"
    In your browser localhost:8000

## Functionallity

Index page will load a random beer each time you access it, if the number of api
calls pass the number of allowed calls then it will get the random beer from the DB.
Also it display a list with brother beers (beers from the same brewery).  
You can access to access to a beer page information just clicking in his name, 
the same for breweries.

Each page has a search box when you can search beers or breweries for name, 
it will display a list of beers of breweries which match with the search name,
also you can display a random beer.

Each time the app display a beer or a brewery, the system will store all beers from the brewery and the brewery
 in the DB, so next time you access it, it wont consume an api call.

When all api calls are consumed the api will only search result in the DB, until
api calls are reset in the next day.

## Logic placement

App\Console\Commands -> Commands to create api entry, and reset api calls

App\Http\Adapter -> Interfaces to make contracts with Factories and repositories used in this app.

App\Http\Controllers -> Controller for this App, just one “BeerController”

App\Http\Entity -> Entities for this App

App\Http\Factory -> Factories to create Beers and Breweries

App\Http\Repository -> 2 kinds to manage DB with doctrine, and the API

App\Http\UseCase -> Case Uses where logic business is stored

beerworld/app/Http/routes.php -> Routing

beerworld/database/mapper
-> XML’s to map entities for doctrine, this way doctrine logic is uncoupled from entities.

beerworld/resources/views -> Templates (Twig) with view logic

beerworld/tests -> Test for cover use cases (PhpUnit + Mockery)

