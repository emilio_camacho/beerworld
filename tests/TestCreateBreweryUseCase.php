<?php

use App\Http\Adapter\Beer\IBeerRepository;
use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\UseCase\IUseCase;
use App\Http\Adapter\Api\IApiRepository;
use App\Http\Entity\Api;
use App\Http\Entity\Brewery;
use App\Http\Entity\Beer;
use App\Http\UseCase\Brewery\Create\CreateBreweryRequest;
use App\Http\UseCase\Brewery\Create\CreateBreweryUseCase;
use App\Http\UseCase\Api\Get\GetApiUseCase;
use App\Http\UseCase\Brewery\Create\CreateBreweryException;

/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 28/08/16
 * Time: 15:21
 */
class TestCreateBreweryUseCase extends PHPUnit_Framework_TestCase
{

    private $createUseCase;
    private $getApiUseCase;
    private $apiRepository;
    private $breweryRepository;
    private $beerRepository;

    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name,$data,$dataName);
        $this->createUseCase = Mockery::mock(IUseCase::class);
        $this->apiRepository = Mockery::mock(IApiRepository::class);
        $this->getApiUseCase = new GetApiUseCase($this->apiRepository);
        $this->breweryRepository = Mockery::mock(IBreweryRepository::class);
        $this->beerRepository = Mockery::mock(IBeerRepository::class);

        $this->breweryRepository
            ->shouldReceive('getById')
            ->andReturn($this->getBrewery())
            ->byDefault();
        $this->beerRepository
            ->shouldReceive('getById')
            ->andReturn($this->getBeer())
            ->byDefault();

        $this->createUseCase
            ->shouldReceive('execute')
            ->andReturn(true)
            ->byDefault();
        $this->apiRepository
            ->shouldReceive('getById')
            ->andReturn($this->getApi())
            ->byDefault();
    }

    public function testCreateOk()
    {
        try {
            $request = $this->buildRequest($this->getBrewery());
            $useCase = $this->buildUseCase();
            $reponse = $useCase->execute($request);
        } catch (\Exception $e) {
            $this->fail("Something happened!");
        }
        $this->assertTrue(true);
    }

    private function buildUseCase()
    {
        return new CreateBreweryUseCase(
            $this->breweryRepository,
            $this->beerRepository
        );
    }

    private function buildRequest(Brewery $brewery)
    {
        return new CreateBreweryRequest($brewery, false);
    }

    private function getApi()
    {
        $api = new Api();
        $api->setId("beer");
        $api->setName("beer");
        $api->setLimit(400);
        $api->setNumCalls(0);
        return $api;
    }

    private function getBrewery()
    {
        $rand = rand(0,1000);
        $brewery = new Brewery();
        $brewery->setName("Brewery " . $rand);
        $brewery->setDescription("Brewery description " . $rand);
        $brewery->setId(uniqid());
        return $brewery;
    }

    private function getBeer()
    {
        $rand = rand(0,1000);
        $beer = new Beer();
        $beer->setId(uniqid());
        $beer->setName("Beer " . $rand);
        $beer->setDescription("Brewery description " . $rand);
        return $beer;
    }


}