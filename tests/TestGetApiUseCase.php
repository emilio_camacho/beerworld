<?php

use App\Http\Adapter\Beer\IBeerRepository;
use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\UseCase\IUseCase;
use App\Http\Adapter\Api\IApiRepository;
use App\Http\Entity\Api;
use App\Http\UseCase\Api\Get\GetApiRequest;
use App\Http\UseCase\Api\Get\GetApiUseCase;
use App\Http\UseCase\Api\Get\GetApiException;

/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 28/08/16
 * Time: 15:21
 */
class TestGetBeerUseCase extends PHPUnit_Framework_TestCase
{
    private $beerApiRepository;
    private $beerDBRepository;
    private $createUseCase;
    private $getApiUseCase;
    private $apiRepository;
    private $breweryDBRepository;

    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name,$data,$dataName);
        $this->beerApiRepository = Mockery::mock(IBeerRepository::class);
        $this->beerDBRepository = Mockery::mock(IBeerRepository::class);
        $this->createUseCase = Mockery::mock(IUseCase::class);
        $this->apiRepository = Mockery::mock(IApiRepository::class);
        $this->getApiUseCase = new GetApiUseCase($this->apiRepository);
        $this->breweryDBRepository = Mockery::mock(IBreweryRepository::class);

        $this->apiRepository
            ->shouldReceive('getById')
            ->andReturn($this->getApi())
            ->byDefault();
    }

    public function testMissingParameters()
    {
        try {
            $request = $this->buildRequest();
            $useCase = $this->buildUseCase();
            $reponse = $useCase->execute($request);
        } catch (GetApiException $e) {
            $this->assertEquals($e->getInternalCode(), GetApiException::API_ID_MISSING);
        }
    }

    public function testApiNotFound()
    {
        $this->apiRepository
            ->shouldReceive('getById')
            ->andReturn(null)
            ->once();
        try {
            $request = $this->buildRequest("asdfa");
            $useCase = $this->buildUseCase();
            $reponse = $useCase->execute($request);
        } catch (GetApiException $e) {
            $this->assertEquals($e->getInternalCode(), GetApiException::API_NOT_FOUND);
        }
    }

    public function testEveryThingOk()
    {
        try {
            $request = $this->buildRequest("beer");
            $useCase = $this->buildUseCase();
            $reponse = $useCase->execute($request);
        } catch (\Exception $e) {
            $this->fail("Something fail!");
        }
        $this->assertTrue(true);
    }
    private function buildUseCase()
    {
        return new GetApiUseCase(
            $this->apiRepository
        );
    }

    private function buildRequest($id = null)
    {
        return new GetApiRequest($id);
    }

    private function getApi()
    {
        $api = new Api();
        $api->setId("beer");
        $api->setName("beer");
        $api->setLimit(400);
        $api->setNumCalls(0);
        return $api;
    }

}