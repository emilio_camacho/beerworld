<?php

use App\Http\Adapter\Beer\IBeerRepository;
use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\UseCase\IUseCase;
use App\Http\Adapter\Api\IApiRepository;
use App\Http\Entity\Api;
use App\Http\Entity\Beer;
use App\Http\UseCase\Beer\Get\GetBeerRequest;
use App\Http\UseCase\Beer\Get\GetBeerUseCase;
use App\Http\UseCase\Api\Get\GetApiUseCase;
use App\Http\UseCase\Beer\Get\GetBeerException;

/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 28/08/16
 * Time: 15:21
 */
class TestGetBeerUseCase extends PHPUnit_Framework_TestCase
{
    private $beerApiRepository;
    private $beerDBRepository;
    private $createUseCase;
    private $getApiUseCase;
    private $apiRepository;
    private $breweryDBRepository;

    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name,$data,$dataName);
        $this->beerApiRepository = Mockery::mock(IBeerRepository::class);
        $this->beerDBRepository = Mockery::mock(IBeerRepository::class);
        $this->createUseCase = Mockery::mock(IUseCase::class);
        $this->apiRepository = Mockery::mock(IApiRepository::class);
        $this->getApiUseCase = new GetApiUseCase($this->apiRepository);
        $this->breweryDBRepository = Mockery::mock(IBreweryRepository::class);


        $this->beerApiRepository
            ->shouldReceive('getById')
            ->andReturn($this->getBeer())
            ->byDefault();
        $this->beerDBRepository
            ->shouldReceive('getById')
            ->andReturn($this->getBeer())
            ->byDefault();
        $this->beerApiRepository
            ->shouldReceive('getByName')
            ->andReturn($this->getPullBeers(5))
            ->byDefault();
        $this->beerDBRepository
            ->shouldReceive('getByName')
            ->andReturn($this->getPullBeers(5))
            ->byDefault();
        $this->createUseCase
            ->shouldReceive('execute')
            ->andReturn(true)
            ->byDefault();
        $this->apiRepository
            ->shouldReceive('getById')
            ->andReturn($this->getApi())
            ->byDefault();
    }

    public function testMissingParameters()
    {
        try {
            $request = $this->buildRequest();
            $useCase = $this->buildUseCase();
            $reponse = $useCase->execute($request);
        } catch (GetBeerException $e) {
            $this->assertEquals($e->getInternalCode(), GetBeerException::PARAMETER_REQUIRED);
        }
    }
    public function testGetByIdFromBDOk()
    {
        $request = $this->buildRequest(uniqid());
        $useCase = $this->buildUseCase();
        $reponse = $useCase->execute($request);

        $this->assertNotEmpty($reponse->getBeers());

        foreach ($reponse->getBeers() as $beer) {
            $this->assertInstanceOf(Beer::class, $beer);
        }
    }

    public function testGetByNameFromBDOk()
    {
        $request = $this->buildRequest(null, "test");
        $useCase = $this->buildUseCase();
        $reponse = $useCase->execute($request);

        $this->assertNotEmpty($reponse->getBeers());

        foreach ($reponse->getBeers() as $beer) {
            $this->assertInstanceOf(Beer::class, $beer);
        }
    }

    public function testGetByIdFromApiOk()
    {
        $this->beerDBRepository
            ->shouldReceive('getById')
            ->andReturn(null)
            ->once();

        $request = $this->buildRequest(uniqid());
        $useCase = $this->buildUseCase();
        $reponse = $useCase->execute($request);

        $this->assertNotEmpty($reponse->getBeers());

        foreach ($reponse->getBeers() as $beer) {
            $this->assertInstanceOf(Beer::class, $beer);
        }
    }

    public function testGetByNameFromApiOk()
    {
        $this->beerDBRepository
            ->shouldReceive('getById')
            ->andReturn(null)
            ->once();

        $request = $this->buildRequest(null, "test");
        $useCase = $this->buildUseCase();
        $reponse = $useCase->execute($request);

        $this->assertNotEmpty($reponse->getBeers());

        foreach ($reponse->getBeers() as $beer) {
            $this->assertInstanceOf(Beer::class, $beer);
        }
    }

    private function buildUseCase()
    {
        return new GetBeerUseCase(
            $this->beerDBRepository,
            $this->beerApiRepository,
            $this->createUseCase,
            $this->getApiUseCase,
            'beer'
        );
    }

    private function buildRequest($id = null, $name = null)
    {
        return new GetBeerRequest($id, $name);
    }

    private function getApi()
    {
        $api = new Api();
        $api->setId("beer");
        $api->setName("beer");
        $api->setLimit(400);
        $api->setNumCalls(0);
        return $api;
    }


    private function getBeer()
    {
        $rand = rand(0,1000);
        $beer = new Beer();
        $beer->setId(uniqid());
        $beer->setName("Beer " . $rand);
        $beer->setDescription("Brewery description " . $rand);
        return $beer;
    }

    private function getPullBeers($num)
    {
        $beers = [];
        for ($i = 0; $i < $num; $i++)
        {
            $beers[] = $this->getBeer();
        }
        return $beers;
    }
}