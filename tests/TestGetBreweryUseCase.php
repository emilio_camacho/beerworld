<?php

use App\Http\Adapter\Beer\IBeerRepository;
use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\UseCase\IUseCase;
use App\Http\Adapter\Api\IApiRepository;
use App\Http\Entity\Api;
use App\Http\Entity\Brewery;
use App\Http\Entity\Beer;
use App\Http\UseCase\Brewery\Get\GetBreweryRequest;
use App\Http\UseCase\Brewery\Get\GetBreweryUseCase;
use App\Http\UseCase\Api\Get\GetApiUseCase;
use App\Http\UseCase\Brewery\Get\GetBreweryException;

/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 28/08/16
 * Time: 15:21
 */
class TestGetBeerUseCase extends PHPUnit_Framework_TestCase
{

    private $createUseCase;
    private $getApiUseCase;
    private $apiRepository;
    private $breweryDBRepository;
    private $breweryApiRepository;

    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name,$data,$dataName);
        $this->createUseCase = Mockery::mock(IUseCase::class);
        $this->apiRepository = Mockery::mock(IApiRepository::class);
        $this->getApiUseCase = new GetApiUseCase($this->apiRepository);
        $this->breweryDBRepository = Mockery::mock(IBreweryRepository::class);
        $this->breweryApiRepository = Mockery::mock(IBreweryRepository::class);

        $this->breweryApiRepository
            ->shouldReceive('getById')
            ->andReturn($this->getBrewery())
            ->byDefault();
        $this->breweryDBRepository
            ->shouldReceive('getById')
            ->andReturn($this->getBrewery())
            ->byDefault();
        $this->breweryApiRepository
            ->shouldReceive('getByName')
            ->andReturn($this->getPullBreweries(5))
            ->byDefault();
        $this->breweryDBRepository
            ->shouldReceive('getByName')
            ->andReturn($this->getPullBreweries(5))
            ->byDefault();

        $this->createUseCase
            ->shouldReceive('execute')
            ->andReturn(true)
            ->byDefault();
        $this->apiRepository
            ->shouldReceive('getById')
            ->andReturn($this->getApi())
            ->byDefault();
    }

    public function testMissingParameters()
    {
        try {
            $request = $this->buildRequest();
            $useCase = $this->buildUseCase();
            $reponse = $useCase->execute($request);
        } catch (GetBreweryException $e) {
            $this->assertEquals($e->getInternalCode(), GetBreweryException::PARAMETER_REQUIRED);
        }
    }

    public function testGetByIdFromBDOk()
    {
        $request = $this->buildRequest(uniqid());
        $useCase = $this->buildUseCase();
        $reponse = $useCase->execute($request);

        $this->assertNotEmpty($reponse->getBreweries());

        foreach ($reponse->getBreweries() as $brewery) {
            $this->assertInstanceOf(Brewery::class, $brewery);
        }
    }

    public function testGetByNameFromBDOk()
    {
        $request = $this->buildRequest(null, "test");
        $useCase = $this->buildUseCase();
        $reponse = $useCase->execute($request);

        $this->assertNotEmpty($reponse->getBreweries());

        foreach ($reponse->getBreweries() as $brewery) {
            $this->assertInstanceOf(Brewery::class, $brewery);
        }
    }

    public function testGetByIdFromApiOk()
    {
        $this->breweryDBRepository
            ->shouldReceive('getById')
            ->andReturn(null)
            ->once();

        $request = $this->buildRequest(uniqid());
        $useCase = $this->buildUseCase();
        $reponse = $useCase->execute($request);

        $this->assertNotEmpty($reponse->getBreweries());

        foreach ($reponse->getBreweries() as $brewery) {
            $this->assertInstanceOf(Brewery::class, $brewery);
        }
    }

    public function testGetByNameFromApiOk()
    {
        $this->breweryDBRepository
            ->shouldReceive('getById')
            ->andReturn(null)
            ->once();

        $request = $this->buildRequest(null, "test");
        $useCase = $this->buildUseCase();
        $reponse = $useCase->execute($request);

        $this->assertNotEmpty($reponse->getBreweries());

        foreach ($reponse->getBreweries() as $brewery) {
            $this->assertInstanceOf(Brewery::class, $brewery);
        }
    }


    private function buildUseCase()
    {
        return new GetBreweryUseCase(
            $this->breweryApiRepository,
            $this->breweryDBRepository,
            $this->createUseCase,
            $this->getApiUseCase,
            $this->apiRepository,
            "beer"
        );
    }

    private function buildRequest($id = null, $name = null)
    {
        return new GetBreweryRequest($id, $name);
    }

    private function getApi()
    {
        $api = new Api();
        $api->setId("beer");
        $api->setName("beer");
        $api->setLimit(400);
        $api->setNumCalls(0);
        return $api;
    }

    private function getBrewery()
    {
        $rand = rand(0,1000);
        $brewery = new Brewery();
        $brewery->setName("Brewery " . $rand);
        $brewery->setDescription("Brewery description " . $rand);
        $brewery->setId(uniqid());
        return $brewery;
    }

    private function getBeer()
    {
        $rand = rand(0,1000);
        $beer = new Beer();
        $beer->setId(uniqid());
        $beer->setName("Beer " . $rand);
        $beer->setDescription("Brewery description " . $rand);
        return $beer;
    }

    private function getPullBreweries($num)
    {
        $breweries = [];
        for ($i = 0; $i < $num; $i++) {
            $breweries[] = $this->getBrewery();
        }
        return $breweries;
    }

}