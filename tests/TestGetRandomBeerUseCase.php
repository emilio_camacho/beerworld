<?php

use App\Http\Adapter\Beer\IBeerRepository;
use App\Http\Adapter\Brewery\IBreweryRepository;
use App\Http\UseCase\IUseCase;
use App\Http\Adapter\Api\IApiRepository;
use App\Http\Entity\Api;
use App\Http\Entity\Beer;
use App\Http\UseCase\Beer\GetRandom\GetRandomBeerRequest;
use App\Http\UseCase\Beer\GetRandom\GetRandomBeerUseCase;
use App\Http\UseCase\Api\Get\GetApiUseCase;
use App\Http\UseCase\Beer\GetRandom\GetRandomBeerException;

/**
 * Created by PhpStorm.
 * User: cerveros
 * Date: 28/08/16
 * Time: 15:21
 */
class TestGetRandomBeerUseCase extends PHPUnit_Framework_TestCase
{
    private $beerApiRepository;
    private $beerDBRepository;
    private $createUseCase;
    private $getApiUseCase;
    private $apiRepository;


    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name,$data,$dataName);
        $this->beerApiRepository = Mockery::mock(IBeerRepository::class);
        $this->beerDBRepository = Mockery::mock(IBeerRepository::class);
        $this->createUseCase = Mockery::mock(IUseCase::class);
        $this->apiRepository = Mockery::mock(IApiRepository::class);
        $this->getApiUseCase = new GetApiUseCase($this->apiRepository);



        $this->beerApiRepository
            ->shouldReceive('getRandom')
            ->andReturn($this->getBeer())
            ->byDefault();
        $this->beerDBRepository
            ->shouldReceive('getRandom')
            ->andReturn($this->getBeer())
            ->byDefault();
        $this->beerDBRepository
            ->shouldReceive('getById')
            ->andReturn($this->getBeer())
            ->byDefault();
        $this->createUseCase
            ->shouldReceive('execute')
            ->andReturn(true)
            ->byDefault();
        $this->apiRepository
            ->shouldReceive('getById')
            ->andReturn($this->getApi())
            ->byDefault();
    }

    public function testEverythingOk()
    {
        $request = $this->buildRequest();
        $useCase = $this->buildUseCase();
        $reponse = $useCase->execute($request);

        $this->assertInstanceOf(Beer::class, $reponse->getBeer());
    }

    public function testForceFoundInDB()
    {
        $api = $this->getApi();
        $api->setNumCalls(500);
        $this->apiRepository
            ->shouldReceive('getById')
            ->andReturn($api)
            ->byDefault();

        $request = $this->buildRequest();
        $useCase = $this->buildUseCase();
        $reponse = $useCase->execute($request);

        $this->assertInstanceOf(Beer::class, $reponse->getBeer());
    }

    public function testBeerNotFoundInApi()
    {
        try {
            $this->beerApiRepository
                ->shouldReceive('getRandom')
                ->andReturn(null)
                ->once();
            $request = $this->buildRequest();
            $useCase = $this->buildUseCase();
            $reponse = $useCase->execute($request);
        } catch (GetRandomBeerException $e) {
            $this->assertEquals(
                $e->getInternalCode(),
                GetRandomBeerException::RANDOM_BEER_NOT_FOUND
            );
        }
    }
    public function testBeerNotFoundInDB()
    {
        try {
            $api = $this->getApi();
            $api->setNumCalls(500);
            $this->apiRepository
                ->shouldReceive('getById')
                ->andReturn($api)
                ->byDefault();
            $this->beerDBRepository
                ->shouldReceive('getRandom')
                ->andReturn(null)
                ->once();
            $request = $this->buildRequest();
            $useCase = $this->buildUseCase();
            $reponse = $useCase->execute($request);
        } catch (GetRandomBeerException $e) {
            $this->assertEquals(
                $e->getInternalCode(),
                GetRandomBeerException::RANDOM_BEER_NOT_FOUND
            );
        }
    }

    private function buildUseCase()
    {
        return new GetRandomBeerUseCase(
            $this->beerApiRepository,
            $this->beerDBRepository,
            $this->createUseCase,
            $this->getApiUseCase,
            'beer'
        );
    }

    private function buildRequest()
    {
        return new GetRandomBeerRequest();
    }

    private function getApi()
    {
        $api = new Api();
        $api->setId("beer");
        $api->setName("beer");
        $api->setLimit(400);
        $api->setNumCalls(0);
        return $api;
    }

    private function getBeer()
    {
        $rand = rand(0,1000);
        $beer = new Beer();
        $beer->setId(uniqid());
        $beer->setName("Beer " . $rand);
        $beer->setDescription("Brewery description " . $rand);
        return $beer;
    }

}